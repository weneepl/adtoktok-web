import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    scrollBehavior(to, from, savedPosition) {
        return {x: 0, y: 0};
    },
    routes: [
        {
            path: '/',
            name: 'index',
            redirect: '/home'
        },
        {
            path: '/home',
            name: 'home',
            component: () => import('~/views/home.vue')
        },
        {
            path: '/auth',
            name: 'auth',
            component: () => import('~/views/auth/index.vue'),
            children: [
                {
                    path: 'login',
                    name: 'authLogin',
                    component: () => import('~/views/auth/login.vue')
                },
                {
                    path: 'signup',
                    name: 'authSignup',
                    component: () => import('~/views/auth/signup.vue')
                },
                // {
                //     path: 'change-password',
                //     name: 'authChangepassword',
                //     component: () => import('~/views/auth/change-password.vue')
                // }
            ]
        },
        {
            path: '/campaign/:id',
            name: 'campaign',
            component: () => import('~/views/auth/index.vue'),
            children: [
                {
                    path: '/',
                    name: 'campaignId',
                    component: () => import('~/views/campaign/id.vue')
                },
                {
                    path: 'apply',
                    name: 'campaignApply',
                    component: () => import('~/views/campaign/apply.vue')
                },
                {
                    path: 'applicant',
                    name: 'campaignApplicant',
                    component: () => import('~/views/campaign/applicant.vue')
                },
                // {
                //     path: 'change-password',
                //     name: 'authChangepassword',
                //     component: () => import('~/views/auth/change-password.vue')
                // }
            ]
        },
        // {
        //     path: '/mypage',
        //     name: 'mypage',
        //     component: () => import('~/views/mypage/index.vue'),
        //     children: [
        //         {
        //             path: '/',
        //             name: 'index',
        //             redirect: '/mypage/account'
        //         },
        //         {
        //             path: '/account',
        //             name: 'account',
        //             component: () => import('~/views/mypage/account.vue')
        //         },
        //     ]
        // },
        {
            path: '/mypage/account',
            name: 'account',
            component: () => import('~/views/mypage/account.vue')
        },
        // {
        //     path: '/campaign/:id/apply',
        //     name: 'campaignApply',
        //     component: () => import('~/views/campaign/apply.vue')
        // },
        {
            path: '*',
            name: 'notFound',
            component: () => import('~/views/not-found.vue')
        }
    ]
});

export default router;
