import Vue from 'vue';

import FileDirective from './file';

// 디렉티브 커스텀 이벤트 타입 export
export * from './file';

// 디렉티브 등록
Vue.directive('file', FileDirective);

console.log('[Directive] insert file');
