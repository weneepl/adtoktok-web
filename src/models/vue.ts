import Vue from 'vue';
import { Modal, ModalDestroy } from '~/plugins/modal';

import { ApiService } from '~/services/api.service';
import { AuthService } from '~/services/auth.service';
import { LayoutService } from '~/services/layout.service';
// import { PayService } from '~/services/pay.service';
// import { AddressService } from '~/services/address.service';

interface SingleTons {
    api: ApiService,
    auth: AuthService,
    layout: LayoutService,
    // pay: PayService,
    // address: AddressService
}

export class BaseVue extends Vue {
    singleTons: SingleTons = this.$root.$data.singleTons;
    $modal: Modal;
}
export class CampaignVue extends BaseVue {
    campaign: any = {};
    get isLogined(): boolean {
        return this.singleTons.auth.isLogined;
    }
    get startDate(): String {
        if(this.campaign['startDate']) return this.campaign['startDate'].substring(5);
        else return '';
    }
    get endDate(): String {
        if(this.campaign['endDate']) return this.campaign['endDate'].substring(5);
        else return '';
    }
    get registerStartDate(): String {
        if(this.campaign['registerStartDate']) return this.campaign['registerStartDate'].substring(5);
        else return '';
    }
    get registerEndDate(): String {
        if(this.campaign['registerEndDate']) return this.campaign['registerEndDate'].substring(5);
        else return '';
    }
    get finalDate(): String {
        if(this.campaign['finalDate']) return this.campaign['finalDate'].substring(5);
        else return '';
    }    
}
export class ModalVue extends Vue {
    destroy: ModalDestroy;
}