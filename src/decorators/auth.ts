import Utils from '~/utils';
import {AuthKey} from '~/services/auth.service'

export default function AuthView(constructor) {
    constructor.prototype['beforeRouteEnter'] = (to, from, next) => {
        const token = Utils.cookie.get(AuthKey.token);
        if (token) {
            next();
            return;
        } else {
            next('/auth/login?redirection=' + to.path);
            return;
        }
    };
}