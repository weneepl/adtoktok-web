import Vue from 'vue'
import App from './App.vue'
import router from './routes';

import { Component } from 'vue-property-decorator';
Component.registerHooks([
	'beforeRouteEnter',
	'beforeRouteUpdate'
]);

//  싱글톤
import api from '~/services/api.service';
import auth from '~/services/auth.service';
import layout from '~/services/layout.service';


//플러그인
import '~/plugins/modal';

// 디렉티브
import '~/directives/index';

import '~/scss/_media.scss'
import '~/scss/main.scss'
import '~/scss/section.scss';

new Vue({
  router,
  el: '#app',
  data: {
    name: 'World',
    singleTons: {
      api,
      auth,
      layout
    }
  },
  render: h => h(App)
})