import { ValidEmail, ValidPassword, ComparePassword, ValidId } from '~/vaildations';

export class SignupForm {

    data = {
        id: '',
        password: '',
        confirmPassword: '',
        name: '',
        email: '',
        // phone: '',
        phone: {
            first: PHONEFIRSTS[0],
            middle: '',
            end: ''
        },
        term: false,
        privacy: false
    };

    validData = () => {
        return {
            id: this.data.id,
            password: this.data.password,
            name: this.data.name,
            email: this.data.email,
            phone: `${this.data.phone.first}-${this.data.phone.middle}-${this.data.phone.end}`
        }
    }

    valids = {
        // id: (): boolean => { return ValidId(this.data.id); },
        email: (): boolean => { return ValidEmail(this.data.email); },
        password: (): boolean => { return ValidPassword(this.data.password); },
        confirmPassword: (): boolean => { return ComparePassword(this.data.password, this.data.confirmPassword); },
        name: (): boolean => {
            if (this.data.name.length > 1) {
                return true;
            } else {
                return false;
            }
        },
        phone: () => {
            if (isNaN(parseInt(this.data.phone.middle))) {
                return false;
            } else if (isNaN(parseInt(this.data.phone.end))) {
                return false;
            } else if (this.data.phone.middle.length !== 4) {
                return false;
            } else if (this.data.phone.end.length !== 4) {
                return false;
            } else {
                return true;
            }
        },
        term: (): boolean => {
            if (this.data.term) {
                return true;
            } else {
                return false;
            }
        },
        privacy: (): boolean => {
            if (this.data.privacy) {
                return true;
            } else {
                return false;
            }
        }
    };

    get getNotValid(): string {
        let notValid:string = '';
        for (let valid in this.valids) {
            if (!this.valids[valid]()) {
                console.log('벨리드', valid);
                notValid = valid;
                break;
            }
        }
        return notValid;
    }

    phoneFirsts: string[] = PHONEFIRSTS;
}

const PHONEFIRSTS: string[] = [
    '010',
    '011',
    '016',
    '017',
    '018',
    '019'
];