import { ValidPhone } from '~/vaildations';

export class AccountForm {

    data = {
        realName: '',
        nickName: '',
        birthYear: -1,
        gender: '',
        phone: '',
        snsUrl: '',
        address: ''
    };

    
    validData = () => {
        return this.data;
    }


    valids = {
        realName: (): boolean => { return this.data.realName.length > 2 },
        nickName: (): boolean => { return this.data.nickName.length > 2 },
        birthYear: (): boolean => { return this.data.birthYear > 0 },
        gender: (): boolean => { return this.data.gender.length > 0 },
        phone: (): boolean => { return ValidPhone(this.data.phone); },
        address: (): boolean => {return this.data.address.length > 0},
    };

    get getNotValid(): string {
        let notValid:string = '';
        for (let valid in this.valids) {
            if (!this.valids[valid]()) {
                console.log('벨리드', valid);
                notValid = valid;
                break;
            }
        }
        return notValid;
    }
}