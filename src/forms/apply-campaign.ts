import { ValidPhone } from '~/vaildations';

export class ApplyForm {

    data = {
        name: '',
        birthYear: -1,
        gender: '',
        phone: '',
        snsUrl: '',
        comment: '',
        option: ''
    };

    
    validData = () => {
        return this.data;
    }


    valids = {
        name: (): boolean => { return this.data.name.length > 2 },
        birthYear: (): boolean => { return this.data.birthYear > 0 },
        gender: (): boolean => { return this.data.gender.length > 0 },
        phone: (): boolean => { return ValidPhone(this.data.phone); },
        comment: (): boolean => {return this.data.comment.length > 0},
        option: (): boolean => {return this.data.option.length > 0},
    };

    get getNotValid(): string {
        let notValid:string = '';
        for (let valid in this.valids) {
            if (!this.valids[valid]()) {
                console.log('벨리드', valid);
                notValid = valid;
                break;
            }
        }
        return notValid;
    }
}