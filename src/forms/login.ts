import { ValidEmail, ValidPassword } from '~/vaildations';

export class LoginForm {

    data = {
        email: '',
        password: ''
    };

    valids = {
        email: (): boolean => { return ValidEmail(this.data.email); },
        password: (): boolean => { return ValidPassword(this.data.password); },
    };

    get getNotValid(): string {
        let notValid:string = '';
        for (let valid in this.valids) {
            if (!this.valids[valid]()) {
                console.log('벨리드', valid);
                notValid = valid;
                break;
            }
        }
        return notValid;
    }
}