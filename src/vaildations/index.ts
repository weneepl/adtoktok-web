export { ValidEmail } from './email';
export { ValidPassword } from './password';
export { ValidId } from './id';
export { ValidPhone } from './phone';
export { ComparePassword } from './compare-password';