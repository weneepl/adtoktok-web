export const ValidPassword = (password: string): boolean => {
    const re = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/;
    if (typeof password !== 'string') {
        return false;
    } else if (re.test(password)) {
        return true;
    } else {
        return false;
    }
};