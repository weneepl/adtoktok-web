export const ValidId = (id: string): boolean => {
    const re = /^[a-zA-Z0-9]{5,}$/;
    if (typeof id !== 'string') {
        return false;
    } else if (re.test(id)) {
        return true;
    } else {
        return false;
    }
}