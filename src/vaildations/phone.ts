export const ValidPhone = (password: string): boolean => {
    const re = /^[0]\d{2}-\d{4}-\d{4}$/;
    if (typeof password !== 'string') {
        return false;
    } else if (re.test(password)) {
        return true;
    } else {
        return false;
    }
};