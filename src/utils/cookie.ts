export default {
    get(key: string): string {
        let ca: string[] = document.cookie.split(';');
        let caLen: number = ca.length;
        let cookieName: string = `${key}=`;
        let c: string;
        for (let i = 0; i < caLen; i += 1) {
            c = ca[i].replace(/^\s+/g, '');
            if (c.indexOf(cookieName) == 0) {
                return c.substring(cookieName.length, c.length);
            }
        }
        return null;
    },
    delete(key: string): void {
        document.cookie = key + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    },
    set(key: string, value: string, expires: number): void {
        let d: Date = new Date();
        d.setTime(d.getTime() + expires);
        document.cookie = `${key}=${value}; expires=${d.toUTCString()}; path=/`;
    },
    setOnlyRunning(key: string, value: string): void {
        document.cookie = `${key}=${value}; expires=; path=/`;
    },
    expires: {
        minute: (value: number) => {
            return (value * 60 * 1000);
        },
        day: (value: number) => {
            return (value * 60 * 60 * 24 * 1000);
        }
    }
}
