export default {
    getItem(key: string): any {
        if (sessionStorage.getItem(key) == 'undefined') {
            return null;
        }
        // if item no exist return null
        return JSON.parse(sessionStorage.getItem(key));
    },
    setItem(key: string, value: any): void {
        sessionStorage.setItem(key, JSON.stringify(value));
    },
    removeItem(key: string): void {
        sessionStorage.removeItem(key);
    }
}
