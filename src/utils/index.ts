import cookie from './cookie';
import session from './session';

export default {
    cookie,
    session
}