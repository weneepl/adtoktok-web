export class LayoutService {
    constructor() {
        console.log('[Layout service] constructor');
    }

    public sidebarShow(): void {
        const sidebar = document.getElementById('sidebar');
        sidebar.style.display = 'block';
        // sidebar.classList.add('sidd')
    }

    public sidebarClose(): void {
        const sidebar = document.getElementById('sidebar');
        sidebar.style.display = 'none';
    }
}

const layoutService = new LayoutService();

export default layoutService;