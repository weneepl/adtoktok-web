import axios from 'axios';
import _api from './api.service';
import Utils from '~/utils';

export const AuthKey = {
    token: 'adtoktok_key'
}

export class AuthService {

    // private _user = {};
    // get user() { return this.user; }
    // set user(user) { this._user = user; }
    public user: any = {};
    private _isLogined: boolean = false;
    get isLogined(): boolean { return this._isLogined; }
    set isLogined(isLogined: boolean) { this._isLogined = isLogined; }
    private _token: string = '';
    get token(): string { return this._token; }
    set token(token: string) { this._token = token; }

    constructor() {
        console.log('[Auth service] constructor');
    }

    public signup(
        email: string,
        password: string,
        nickname: string,
        phone: string
    ): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            const url = `${_api.endPoint}/api/auth/signup`;
            const body = {
                email: email,
                password: password,
                nickname: nickname,
                phone: phone
            };
            axios.post(url, body).then((res) => {
                console.log('[Auth service] signup - ', res);
                if (res.data.success) {
                    this.user = res.data.user;
                    this.isLogined = true;
                    Utils.session.setItem(AuthKey.token, res.data.token);
                    Utils.cookie.setOnlyRunning(AuthKey.token, res.data.token);
                    _api.setAxiosGlobalConfig(res.data.token);
                    this.token = res.data.token;
                    resolve();
                } else {
                    if (res.data.err === 'duplicated_email') {
                        reject('이미 가입된 이메일입니다.');
                    } else {
                        reject('잠시 후에 다시 시도해주세요.')
                    }
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public login(email: string, password: string) {
        return new Promise<void>((resolve, reject) => {
            const url = `${_api.endPoint}/api/auth/login`;
            const body = {
                email: email,
                password: password
            }
            axios.post(url, body).then((res) => {
                console.log('[Auth service] login - ', res);
                if (res.data.success) {
                    this.user = res.data.user;
                    _api.setAxiosGlobalConfig(res.data.token);
                    Utils.session.setItem(AuthKey.token, res.data.token);
                    Utils.cookie.set(AuthKey.token, res.data.token, Utils.cookie.expires.day(30));
                    this.token = res.data.token;
                    this.isLogined = true;
                    resolve();
                } else {
                    if (res.data.err === 'not_existed') {
                        reject('없는 회원입니다.');
                    } else if (res.data.err === 'invalid_password') {
                        reject('비밀번호가 틀렸습니다.');
                    } else {
                        reject('잠시 후 다시 시도해주세요.');
                    }
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public tokenLogin() {
        return new Promise<void>((resolve, reject) => {
            const token: string = Utils.cookie.get(AuthKey.token);
            console.log('토큰을 보자', token);
            if (token) {
                const url: string = `${_api.endPoint}/api/auth/token-login`;
                const options = {
                    headers: {
                        'Access-Control-Allow-Headers': 'x-access-token',
                        'x-access-token': token
                    }
                };
                axios.get(url, options).then((res) => {
                    console.log('[Auth service] token login - ', res);
                    if (res.data.success) {
                        this.user = res.data.user;
                        _api.setAxiosGlobalConfig(res.data.token);
                        Utils.session.setItem(AuthKey.token, res.data.token);
                        Utils.cookie.set(AuthKey.token, res.data.token, Utils.cookie.expires.day(30));
                        this.token = res.data.token;
                        this.isLogined = true;
                        resolve();
                    } else {
                        reject();
                    }
                }).catch((err) => {
                    reject();
                });
            } else {
                reject();
            }
        });
    }

    public refreshProfile(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${_api.endPoint}/api/auth/profile`;
            axios.get(url).then((res) => {
                if (res.data.success) {
                    this.user = res.data.user;
                    _api.setAxiosGlobalConfig(res.data.token);
                    Utils.session.setItem(AuthKey.token, res.data.token);
                    Utils.cookie.set(AuthKey.token, res.data.token, Utils.cookie.expires.day(30));
                    this.token = res.data.token;
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            })
        });
    }

    public changePassword(oldPassword: string, newPassword: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            const url: string = `${_api.endPoint}/api/auth/change-password`;
            const body = {
                oldPassword,
                newPassword
            };
            axios.post(url, body).then((res) => {
                if (res.data.success) {
                    resolve();
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public loginWithOrderId(orderId: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            const url: string = `${_api.endPoint}/api/auth/no-member-login`;
            const body = {
                orderId
            };
            axios.post(url, body).then((res) => {
                if (res.data.success) {
                    resolve();
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public logout(): Promise<any> {
        return new Promise<any>((resolve) => {
            this.user = {};
            this.isLogined = false;
            _api.deleteAxiosGlobalConfig();
            Utils.cookie.delete(AuthKey.token);
            Utils.session.removeItem(AuthKey.token);
            resolve();
        });
    }
}

const authService = new AuthService();

export default authService;
