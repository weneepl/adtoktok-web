import axios from 'axios';
import { LIST } from '~/data/mock';

export class ApiService {

    get endPoint(): string { return process.env.ROOT_API; }

    public setAxiosGlobalConfig(token: string): void {
        axios.defaults.headers['x-access-token'] = token;
        axios.defaults.headers['Access-Control-Allow-Headers'] = 'x-access-token';
    }

    public deleteAxiosGlobalConfig(): void {
        axios.defaults.headers['x-access-token'] = '';
        axios.defaults.headers['Access-Control-Allow-Headers'] = '';
    }

    constructor() {
        console.log('[Api service] constructor');
        axios.defaults.headers['content-type'] = 'application/json;charset=utf-8';
    }

    public getCurrentHotItems() {
        return new Promise<any[]>((resolve, reject) => {
            // resolve(LIST);
            const url: string = `${this.endPoint}/api/main/hotlist`;
            axios.get(url).then((res) => {
                console.log('[Api service] getCurrentHotItems : ', res.data);
                if (res.data.success) {
                    resolve(res.data.campaigns);
                    for(let i = 0; i < res.data.campaigns.length; i++) {
                        res.data.campaigns[i]['remainDate'] = 3;
                        res.data.campaigns[i]['curApply'] = 1;
                    }
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public applyCampaign(id, applyInfo) {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/campaign/${id}/apply`;
            console.log('url : ', url);
            const body = {
                name: applyInfo.name,
                birthYear: applyInfo.birthYear,
                gender: applyInfo.gender,
                phone: applyInfo.phone,
                snsUrl: applyInfo.snsUrl,
                comment: applyInfo.comment,
                option: applyInfo.option,
            };
            axios.post(url, body).then((res) => {
                console.log('[Api service] apply Campaign - ', res);
                if (res.data.success) {
                    resolve();
                } else {
                    if (res.data.err === 'already_applied') {
                        reject('이미 신청한 캠페인입니다.');
                    } else {
                        reject('잠시 후 다시 시도해주세요.');
                    }
                }
            }).catch((err) => {
                reject(err);
            });
        });
    }


    public getAllCategories() {
        return new Promise<any[]>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/category/list`;
            axios.get(url).then((res) => {
                console.log('[Api service] getAllCategories : ', res.data);
                if (res.data.success) {
                    resolve(res.data.categories);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }


    public getSubCategoryWithParent(id: number) {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/sub-category/${id}`;
            axios.get(url).then((res) => {
                if (res.data.success) {
                    resolve({
                        main: res.data.main,
                        sub: res.data.sub
                    });
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public getRandomSubCategories(): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/home/hot`;
            axios.get(url).then((res) => {
                if (res.data.success) {
                    resolve(res.data.subCategories);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public getRandomSubCategoriesMusical(): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/home/musical`;
            axios.get(url).then((res) => {
                if (res.data.success) {
                    resolve(res.data.subCategories);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public getRandomSubCategoriesConcert(): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/home/concert`;
            axios.get(url).then((res) => {
                if (res.data.success) {
                    resolve(res.data.subCategories);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public getCampaignApplicants(id: number): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/campaign/${id}/applicants`;

            axios.get(url).then((res) => {
                console.log('[Api service] get campaign applicants -', res);
                if (res.data.success) {
                    resolve(res.data.applicants);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        })
    }

    public getCampaignById(id: number): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/campaign/${id}`;
            axios.get(url).then((res) => {
                console.log('[Api service] get campaign by id -', res);
                if (res.data.success) {
                    resolve(res.data);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        })
    }

    public uploadFile(file: File): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/image/upload`;
            const formData = new FormData();
            formData.append('images', file);
            axios.post(url, formData).then((res) => {
                console.log('[Api service] upload file - ', res);
                if (res.data.success) {
                    resolve(res.data.url);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject(err);
            });
        });
    }

    public uploadProduct(
        title: string,
        mainCategoryId: number,
        subCategoryId: number,
        seatInfoZone: string,
        seatInfoColumn: string,
        seatInfoDesc: string,
        usedDate: Date,
        quantity: number,
        price: number,
        ageType: number,
        ageValue: number,
        content: string,
        images: string[],
        dealSystemPinUsed: boolean,
        dealSystemDeliveryUsed: boolean,
        dealSystemDeliveryPayer: number,
        dealSystemDeliveryPrice: number,
        dealSysyemPlaceUsed: boolean,
        dealSystemPlaceDesc: string
    ): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/product`;
            const body = {
                title: title,
                mainCategoryId: mainCategoryId,
                subCategoryId: subCategoryId,
                seatInfoZone: seatInfoZone,
                seatInfoColumn: seatInfoColumn,
                seatInfoDesc: seatInfoDesc,
                usedDate: usedDate,
                quantity: quantity,
                price: price,
                ageType: ageType,
                ageValue: ageValue,
                content: content,
                images: images,
                dealSystemPinUsed: dealSystemPinUsed,
                dealSystemDeliveryUsed: dealSystemDeliveryUsed,
                dealSystemDeliveryPayer: dealSystemDeliveryPayer,
                dealSystemDeliveryPrice: dealSystemDeliveryPrice,
                dealSysyemPlaceUsed: dealSysyemPlaceUsed,
                dealSystemPlaceDesc: dealSystemPlaceDesc
            };
            axios.post(url, body).then((res) => {
                console.log('[Api service] upload product - ', res);
                if (res.data.success) {
                    resolve(res.data.product);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject(err);
            });
        });
    }

    public getProducts(): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/product/list`;
            axios.get(url).then((res) => {
                if (res.data.success) {
                    resolve(res.data.products);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject(err);
            });
        });
    }

    public getProductsByOwner(): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/product/owner`;
            axios.get(url).then((res) => {
                console.log('[Api service] get products by owner - ', res);
                if (res.data.success) {
                    resolve(res.data.products);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            })
        });
    }

    public getProductBySubcategory(id: number): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/product/list-subcategory?id=${id}`;
            axios.get(url).then((res) => {
                console.log('[Api service] get products by subcategory - ', res);
                if (res.data.success) {
                    resolve(res.data.products);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject(err);
            })
        });
    }

    public getProductWithOrder(id: number): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/product/${id}/order`;
            axios.get(url).then((res) => {
                if (res.data.success) {
                    resolve(res.data.product);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject(err);
            });
        });
    }

    public createOrder(productId: number, system: string, depositor: string, delivery: any = null): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/order`;
            const body = {
                productId: productId,
                system: system,
                depositor: depositor,
                delivery: delivery
            };
            console.log('body', body);
            axios.post(url, body).then((res) => {
                console.log('[Api service] create order - ', res);
                if (res.data.success) {
                    resolve(res.data.order);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public getOrderWithOrderId(orderId: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/order?orderId=${orderId}`;
            axios.get(url).then((res) => {
                if (res.data.success) {
                    resolve(res.data.order);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public createNoMemberOrder(productId: number, system: string, depositor: string, delivery: any = null): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/order/no-member`;
            const body = {
                productId: productId,
                system: system,
                depositor: depositor,
                delivery: delivery
            };
            axios.post(url, body).then((res) => {
                if (res.data.success) {
                    resolve(res.data.order);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public getOrderByOrderId(orderId: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/order?orderId=${orderId}`;
            axios.get(url).then((res) => {
                console.log('[Api service] get order by orderId - ', res);
                if (res.data.success) {
                    resolve(res.data.order);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            })
        });
    }

    public getOrders(): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/order/owner`;
            axios.get(url).then((res) => {
                if (res.data.success) {
                    resolve(res.data.orders);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            })
        });
    }

    public confirmOrder(orderDocId: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/order/${orderDocId}/confirm`;
            axios.post(url, {}).then((res) => {
                if (res.data.success) {
                    resolve();
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public savePin(id: string, pin: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/order/${id}/save-pin`;
            const body = {
                pin
            };
            axios.post(url, body).then((res) => {
                if (res.data.success) {
                    resolve(res.data.order);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public saveInvoiceNumber(id: string, invoiceNumber: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/order/${id}/save-invoice-number`;
            const body = {
                invoiceNumber
            };
            axios.post(url, body).then((res) => {
                if (res.data.success) {
                    resolve(res.data.order);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public getNotices(): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/notice/list`;
            axios.get(url).then((res) => {
                if (res.data.success) {
                    res.data.notices.forEach((item) => {
                        item['$toggle'] = false;
                    });
                    resolve(res.data.notices);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            })
        });
    }

    public createLogExchange(mileage: number, bankName: string, bankNumber: string, depositor: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/log-exchange`;
            const body = {
                mileage,
                bankName,
                bankNumber,
                depositor
            };
            axios.post(url, body).then((res) => {
                if (res.data.success) {
                    resolve(res.data.logExchange);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public getLogExchangeWithOwner(): Promise<any[]> {
        return new Promise<any[]>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/log-exchange/owner`;
            axios.get(url).then((res) => {
                if (res.data.success) {
                    resolve(res.data.logExchanges);
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }

    public createLogPage(pageName: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            const url: string = `${this.endPoint}/api/log-page`;
            const body = {
                pageName
            };
            axios.post(url, body).then((res) => {
                console.log('[Api service] get log -', res);
                if (res.data.success) {
                    resolve();
                } else {
                    reject();
                }
            }).catch((err) => {
                reject();
            });
        });
    }
}

const apiService = new ApiService();

export default apiService;
